-- Greeting on initialize
greeting = {
	"Beautiful day isn't it...",
	"Nice to see you again...",
	"Welcome back. I missed you...",
	"You're back...",
	"Thank you for bringing me to this world"
	}

-- Stage 1
poke_dialogue_1 = {
	"A crude display of love",
	"You're still childish",
	"That's enough",
	"Is this mingling, too?",
	"You never change.",
	"You're so weird.",
	"Don't worry, I'm here.",
	"Pervert...",
	"Behave yourself, okay?",
	"Terrible, as usual.",
	"Still at it?",
	"Did something happen?",
	"Your heart's not into it.",
	"You're distracting me."
	}
	
-- Stage 2, looks up at you 
poke_dialogue_2 = {
	"You're the actual worst",
	"I'm done.",
	"Even I have my dislikes.",
	"Reality not cutting it?",
	"Enough touching.",
	"Ever heard of limits?",
	"Ugh, that's unpleasant.",
	"Don't get carpal tunnel.",
	"Don't look so into it...",
	"You're worst than that Facade."
	}

-- Stage 3: You made her cry >:(
-- I had to make her cry so many times and it hurts
poke_dialogue_3 = {
	"Stop... Please...",
	"I can't take it anymore.",
	"What do you need?",
	"I'll do anything.",
	"I love you too, I promise.",
	"You truly know no limits.",
	"Leave me alone, for now..."
	}

-- When the skin is launched/refreshed
function Initialize()
	-- This function apparently is the first thing that runs when loading the skin, making the code below always return nil
	
	--[[local meter_greeting = SKIN:GetMeter('MeterGreeting')
	--local name = meter_greeting:GetOption(Text, 'pp')
	local name = SELF:GetOption(Text, 'kk')
	print(name)]]--
	
	local idx = math.random(1, #greeting)
	local quote = greeting[idx]
	--quote = string.gsub(quote, "NAME", name)
	
	SKIN:Bang('!SetOption', 'MeterText', 'Hidden', 0)
	SKIN:Bang('!SetOption', 'MeterText', 'Text', quote)
	
	SKIN:Bang('!SetOption', 'MeterSpeech', 'Hidden', 0)
	
	SKIN:Bang('!UpdateMeter', 'MeterSpeech')
	SKIN:Bang('!UpdateMeter', 'MeterText')
	
	SKIN:Bang('!CommandMeasure', 'MeasureTimer', 'Stop 1')
	SKIN:Bang('!CommandMeasure', 'MeasureTimer', 'Execute 1')
end

-- Runs every update specified under [MeasureScript]
function Update()
	local value = SKIN:GetVariable('taps')
	
	-- Once Es calms down from crying, she starts reading again, but she's still sensitive from what you did to her
	if (tonumber(value) < 100) then
		SKIN:Bang('!SetOption', 'MeterEsCry', 'Hidden', 1)
		SKIN:Bang('!SetOption', 'MeterEs1', 'ImageAlpha', 255)
		
		SKIN:Bang('!UpdateMeter', 'MeterEsCry')
		SKIN:Bang('!UpdateMeter', 'MeterEs1')
	end
	
	if (tonumber(value) > 0) then
		SKIN:Bang('!CommandMeasure', 'MeasureTimer', 'Execute 2')
	end
end

function alpha(value)
	print(value)
end

function poke(tap)
	--local number = tap + 1 --debug
	--print(number)
	
	SKIN:Bang('!SetOption', 'MeterSpeech', 'Hidden', 0)
	SKIN:Bang('!UpdateMeter', 'MeterSpeech')
	
	
	if (tap >= 100) then
		-- Es shows pain
		
		local idx = math.random(1, #poke_dialogue_3)
		local quote = poke_dialogue_3[idx]
		
		-- Reveals text bubble
		SKIN:Bang('!SetOption', 'MeterText', 'Hidden', 0)
		SKIN:Bang('!SetOption', 'MeterText', 'Text', quote)
		
		-- Shows crying Es :(
		SKIN:Bang('!SetOption', 'MeterEsCry', 'Hidden', 0)
		
		-- Stops and hide reading animation. Alpha set to 1 to allow clicking. Shouldn't be noticeable
		SKIN:Bang('!SetOption', 'MeterEs1', 'ImageAlpha', 1)
		SKIN:Bang('!SetOption', 'MeterEs2', 'Hidden', 1)
		
		-- Hides talk image
		SKIN:Bang ('!SetOption', 'MeterEsTalk', 'Hidden', 1)

		-- Updates all changes
		SKIN:Bang('!UpdateMeter', 'MeterEsCry')
		SKIN:Bang('!UpdateMeter', 'MeterEsTalk')
		SKIN:Bang('!UpdateMeter', 'MeterEs1')
		SKIN:Bang('!UpdateMeter', 'MeterEs2')
		SKIN:Bang('!UpdateMeter', 'MeterText')
		
	elseif (tap < 100 and tap > 15) then
		-- When Es looks up at you
		
		--print("HI")
		local idx = math.random(1, #poke_dialogue_2)
		local quote = poke_dialogue_2[idx]

		SKIN:Bang('!SetOption', 'MeterText', 'Hidden', 0)
		SKIN:Bang('!SetOption', 'MeterText', 'Text', quote)
		
		SKIN:Bang('!SetOption', 'MeterEsTalk', 'Hidden', 0)
		SKIN:Bang('!SetOption', 'MeterEs2', 'Hidden', 1)	
		
		SKIN:Bang('!UpdateMeter', 'MeterEsTalk')
		SKIN:Bang('!UpdateMeter', 'MeterEs2')
		SKIN:Bang('!UpdateMeter', 'MeterText')
		
	else
		-- Es doesn't react other than speaking
		local idx = math.random(1, #poke_dialogue_1)
		local quote = poke_dialogue_1[idx]

		SKIN:Bang('!SetOption', 'MeterText', 'Hidden', 0)
		SKIN:Bang('!SetOption', 'MeterText', 'Text', quote)
		
		SKIN:Bang('!UpdateMeter', 'MeterText')
	end
	
	-- Revert everything back to idle animation
	SKIN:Bang('!CommandMeasure', 'MeasureTimer', 'Stop 1')
	SKIN:Bang('!CommandMeasure', 'MeasureTimer', 'Execute 1')
end

