# Es on your desktop

**Es, the pictures used for the wallpaper, and the speech bubbles were all created by the makers of Alter Ego, Caramel Column Inc.**

Ever wished Es was part of your computer? Now you can with the powers of Rainmeter!

#### Functionality:
* Es greets you at start
* Es responds to you poking her
* Don't poke her too much; it's uncomfortable

#### Compatibility:
The skin should work with any version of Windows 10 and Rainmeter 4.3.1.3321 or above. It only uses plugins that come pre-installed with Rainmeter and doesn't request any resources outside of the [at]Resources folder. Rainmeter handles the Lua script

.xcf files were made on GIMP 2.10.20

#### How to install:
1. Download and install [Rainmeter](https://www.rainmeter.net/) if you don't have it installed.
2. Download the rmskin file
3. Run the rmskin file. It should install the skin for you.
4. The skin should load once installation is finished, but if it doesn't open Rainmeter, navigate to "alter_ego", select es.ini or es_speed_read.ini, then click load.
5. Enjoy Es on your desktop.

#### Improvements:
All you need is a text editor (preferably once that can syntax highlight Lua and .ini files like [Notepad++](https://notepad-plus-plus.org/))

I don't plan on adding more functionality to this skin, but if you want to modify any part of this project, feel free to post on the r/TheTempleofEs subreddit. I tried to document each part as well as I could.

I don't know how to use Git that well and this was mainly a learning project for me.

Masking Es and making the wallpapers were all done with GIMP 2.10.20
Yes I know the wallpaper sucks

#### Closing
Thank you for taking the time to look at this project I made. Although some of the features I wanted had to be scrapped due to reasons I don't want to talk about, I'm proud of what I made. If you do install this skin, tell Es I'm sorry for making her endure all that, will you?


HMitsuha 2021.04.04

P.S. I also have Es on my iPhone using a jailbreak tweak. If you want to know how, then let me know and I'll make another post about it. Jailbreaking does affect the functionality of some apps (like banking apps and Snapchat) though.
