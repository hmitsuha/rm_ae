[Variables]
; Change the widths to resize
; Change XSpeech, YSpeech, and Fsize so text and speech bubble are where you want it

WEs = 620
HEs = [AspectRatioEs]

WSpeech = 581
HSpeech = [AspectRatioSpeech]

XSpeech = 500
YSpeech = 0

WText = #WSpeech# * 0.1
HText = #HSpeech# * 0.5

FSize = 28


[AspectRatioEs]
; Calculates Height while retaining aspect ratio. Original dimensions are 620x984

Measure = Calc
Formula = (984 / 620) * #WEs#

UpdateDivider = -1

[AspectRatioSpeech]
; Same, but for speech bubble. Original is 297x581

Measure = Calc
Formula = (297 / 581) * #WSpeech#

UpdateDivider = -1

[CenterX]
Measure = Calc
Formula = #WSpeech# / 2

UpdateDivider = -1

[Resize]
W = #WEs#
H = #HEs#

[SpeechDimensions]
X = #XSpeech#r
Y = #YSpeech#r

W = #WSpeech#
H = #HSpeech#

[Text]
FontSize = #FSize#
X = [CenterX]r
Y = ([AspectRatioSpeech] * 0.4)r

W = #WText#
H = #HText#